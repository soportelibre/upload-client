package ec.gob.funcionjudicial.upload;

import java.io.File;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

/**
 * Cliente REST para carga de archivos.
 * 
 * @author Ricardo Arguello <ricardo.arguello@soportelibre.com>
 */
public class UploadClient {

    private static final String URL_REST = "http://localhost:8080/upload-service/rest/files/upload";
    private static final File UPLOAD_FILE = new File("/var/tmp/test.pdf");
    private static final String FORM_PARAMETER = "archivo";

    public static void main(String[] args) throws Exception {
        try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
            HttpPost httpPost = new HttpPost(URL_REST);
            FileBody fileBody = new FileBody(UPLOAD_FILE);

            HttpEntity reqEntity = MultipartEntityBuilder.create().addPart(FORM_PARAMETER, fileBody).build();
            httpPost.setEntity(reqEntity);

            try (CloseableHttpResponse response = httpClient.execute(httpPost)) {
                HttpEntity resEntity = response.getEntity();
                System.out.println(EntityUtils.toString(resEntity));
                EntityUtils.consume(resEntity);
            }
        }
    }
}